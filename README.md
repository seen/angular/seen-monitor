# Seen Monitor

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-monitor/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-monitor/commits/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/32153c6cc0a54869b71f241a9d512b5d)](https://www.codacy.com/app/seen/seen-monitor?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-monitor&amp;utm_campaign=Badge_Grade)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-monitor/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-monitor/commits/develop)